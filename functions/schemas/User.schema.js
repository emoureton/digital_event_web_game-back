import mongoose from "mongoose";

const UserSchema = new mongoose.Schema({
  cleanedPseudo: {
    type: String,
    unique: true,
    required: true
  },
  pseudo: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  score: {
    type: Number,
    default: 0
  },
  token: {
    type: String,
  },
});

export const User = mongoose.model("User", UserSchema);