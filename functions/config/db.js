import mongoose from "mongoose";

export const connectDB = async () => {
	try {
		const conn = await mongoose.connect("mongodb+srv://haltoadmin:WzHZro892JqHHPv8hqMdSZGtus8p@cluster0.eioaj.mongodb.net/halto?retryWrites=true&w=majority", {
			useNewUrlParser: true,
      useUnifiedTopology: true,
			autoIndex: true,
		});
		console.log(`MongoDB connected: ${conn.connection.host}`);
	} catch (error) {
		console.error(error.message);
		process.exit(1);
	}
};