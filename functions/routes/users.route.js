import express from 'express'
import { createUser, getUsers, getUser, updateUser, deleteUser } from '../controllers/users.controller.js'
import { login } from '../controllers/login.controller.js';

export const router = express.Router();

router.route('/')
  .get(getUsers)
  .post(createUser);

router.route('/:id')
  .get(getUser)
  .put(updateUser)
  .delete(deleteUser);

router.route('/login/:id')
  .post(login)
