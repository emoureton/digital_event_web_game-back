import dotenv from 'dotenv';
import express from 'express';
import bodyParser from 'body-parser';
import { router as usersRoute } from './routes/users.route.js';
import { router as roomsRoute } from './routes/rooms.route.js';
import { connectDB } from "./config/db.js";
import functions from "firebase-functions";
import cors from 'cors'

dotenv.config();
connectDB();

const app = express();
const PORT = 5000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(cors({
  origin: '*'
}))
app.options(
  cors({
    origin: true,
    optionsSuccessStatus: 200
  })
);

app.use('/users', usersRoute);
app.use('/rooms', roomsRoute);

app.listen(PORT, () => {console.log("Connected at PORT : " + PORT)});

export const halto = functions.https.onRequest((req, res) => {
	if (!req.path) {
		req.url = `/${req.url}`
	}
	return app(req, res)
})
